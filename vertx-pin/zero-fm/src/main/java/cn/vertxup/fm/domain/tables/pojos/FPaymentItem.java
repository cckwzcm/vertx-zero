/*
 * This file is generated by jOOQ.
 */
package cn.vertxup.fm.domain.tables.pojos;


import cn.vertxup.fm.domain.tables.interfaces.IFPaymentItem;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.math.BigDecimal;
import java.time.LocalDateTime;


import static io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo.*;
/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class FPaymentItem implements VertxPojo, IFPaymentItem {

    private static final long serialVersionUID = 1L;

    private String        key;
    private String        name;
    private String        code;
    private String        serial;
    private BigDecimal    amount;
    private BigDecimal    amountPre;
    private String        settlementId;
    private String        paymentId;
    private String        payName;
    private String        payMobile;
    private String        payMethod;
    private String        payId;
    private String        sigma;
    private String        language;
    private Boolean       active;
    private String        metadata;
    private LocalDateTime createdAt;
    private String        createdBy;
    private LocalDateTime updatedAt;
    private String        updatedBy;

    public FPaymentItem() {}

    public FPaymentItem(IFPaymentItem value) {
        this.key = value.getKey();
        this.name = value.getName();
        this.code = value.getCode();
        this.serial = value.getSerial();
        this.amount = value.getAmount();
        this.amountPre = value.getAmountPre();
        this.settlementId = value.getSettlementId();
        this.paymentId = value.getPaymentId();
        this.payName = value.getPayName();
        this.payMobile = value.getPayMobile();
        this.payMethod = value.getPayMethod();
        this.payId = value.getPayId();
        this.sigma = value.getSigma();
        this.language = value.getLanguage();
        this.active = value.getActive();
        this.metadata = value.getMetadata();
        this.createdAt = value.getCreatedAt();
        this.createdBy = value.getCreatedBy();
        this.updatedAt = value.getUpdatedAt();
        this.updatedBy = value.getUpdatedBy();
    }

    public FPaymentItem(
        String        key,
        String        name,
        String        code,
        String        serial,
        BigDecimal    amount,
        BigDecimal    amountPre,
        String        settlementId,
        String        paymentId,
        String        payName,
        String        payMobile,
        String        payMethod,
        String        payId,
        String        sigma,
        String        language,
        Boolean       active,
        String        metadata,
        LocalDateTime createdAt,
        String        createdBy,
        LocalDateTime updatedAt,
        String        updatedBy
    ) {
        this.key = key;
        this.name = name;
        this.code = code;
        this.serial = serial;
        this.amount = amount;
        this.amountPre = amountPre;
        this.settlementId = settlementId;
        this.paymentId = paymentId;
        this.payName = payName;
        this.payMobile = payMobile;
        this.payMethod = payMethod;
        this.payId = payId;
        this.sigma = sigma;
        this.language = language;
        this.active = active;
        this.metadata = metadata;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

        public FPaymentItem(io.vertx.core.json.JsonObject json) {
                this();
                fromJson(json);
        }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.KEY</code>. 「key」- 收款明细账单主键ID
     */
    @Override
    public String getKey() {
        return this.key;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.KEY</code>. 「key」- 收款明细账单主键ID
     */
    @Override
    public FPaymentItem setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.NAME</code>. 「name」 - 收款明细单标题
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.NAME</code>. 「name」 - 收款明细单标题
     */
    @Override
    public FPaymentItem setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CODE</code>. 「code」 - 收款明细单编号
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CODE</code>. 「code」 - 收款明细单编号
     */
    @Override
    public FPaymentItem setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SERIAL</code>. 「serial」 -
     * 收款明细单据号
     */
    @Override
    public String getSerial() {
        return this.serial;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SERIAL</code>. 「serial」 -
     * 收款明细单据号
     */
    @Override
    public FPaymentItem setSerial(String serial) {
        this.serial = serial;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.AMOUNT</code>.
     * 「amount」——价税合计，所有明细对应的实际结算金额
     */
    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.AMOUNT</code>.
     * 「amount」——价税合计，所有明细对应的实际结算金额
     */
    @Override
    public FPaymentItem setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.AMOUNT_PRE</code>.
     * 「amountPre」预付金额
     */
    @Override
    public BigDecimal getAmountPre() {
        return this.amountPre;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.AMOUNT_PRE</code>.
     * 「amountPre」预付金额
     */
    @Override
    public FPaymentItem setAmountPre(BigDecimal amountPre) {
        this.amountPre = amountPre;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SETTLEMENT_ID</code>.
     * 「settlementId」结算单ID
     */
    @Override
    public String getSettlementId() {
        return this.settlementId;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SETTLEMENT_ID</code>.
     * 「settlementId」结算单ID
     */
    @Override
    public FPaymentItem setSettlementId(String settlementId) {
        this.settlementId = settlementId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAYMENT_ID</code>.
     * 「paymentId」收款单ID
     */
    @Override
    public String getPaymentId() {
        return this.paymentId;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAYMENT_ID</code>.
     * 「paymentId」收款单ID
     */
    @Override
    public FPaymentItem setPaymentId(String paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_NAME</code>.
     * 「payName」打款人姓名
     */
    @Override
    public String getPayName() {
        return this.payName;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_NAME</code>.
     * 「payName」打款人姓名
     */
    @Override
    public FPaymentItem setPayName(String payName) {
        this.payName = payName;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_MOBILE</code>.
     * 「payMobile」打款人电话
     */
    @Override
    public String getPayMobile() {
        return this.payMobile;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_MOBILE</code>.
     * 「payMobile」打款人电话
     */
    @Override
    public FPaymentItem setPayMobile(String payMobile) {
        this.payMobile = payMobile;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_METHOD</code>.
     * 「payMethod」付款方式
     */
    @Override
    public String getPayMethod() {
        return this.payMethod;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_METHOD</code>.
     * 「payMethod」付款方式
     */
    @Override
    public FPaymentItem setPayMethod(String payMethod) {
        this.payMethod = payMethod;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_ID</code>. 「payId」付款账号
     */
    @Override
    public String getPayId() {
        return this.payId;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.PAY_ID</code>. 「payId」付款账号
     */
    @Override
    public FPaymentItem setPayId(String payId) {
        this.payId = payId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public String getSigma() {
        return this.sigma;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public FPaymentItem setSigma(String sigma) {
        this.sigma = sigma;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.LANGUAGE</code>. 「language」-
     * 使用的语言
     */
    @Override
    public String getLanguage() {
        return this.language;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.LANGUAGE</code>. 「language」-
     * 使用的语言
     */
    @Override
    public FPaymentItem setLanguage(String language) {
        this.language = language;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public Boolean getActive() {
        return this.active;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public FPaymentItem setActive(Boolean active) {
        this.active = active;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.METADATA</code>. 「metadata」-
     * 附加配置数据
     */
    @Override
    public String getMetadata() {
        return this.metadata;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.METADATA</code>. 「metadata」-
     * 附加配置数据
     */
    @Override
    public FPaymentItem setMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CREATED_AT</code>.
     * 「createdAt」- 创建时间
     */
    @Override
    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CREATED_AT</code>.
     * 「createdAt」- 创建时间
     */
    @Override
    public FPaymentItem setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CREATED_BY</code>.
     * 「createdBy」- 创建人
     */
    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.CREATED_BY</code>.
     * 「createdBy」- 创建人
     */
    @Override
    public FPaymentItem setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.UPDATED_AT</code>.
     * 「updatedAt」- 更新时间
     */
    @Override
    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.UPDATED_AT</code>.
     * 「updatedAt」- 更新时间
     */
    @Override
    public FPaymentItem setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.F_PAYMENT_ITEM.UPDATED_BY</code>.
     * 「updatedBy」- 更新人
     */
    @Override
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.F_PAYMENT_ITEM.UPDATED_BY</code>.
     * 「updatedBy」- 更新人
     */
    @Override
    public FPaymentItem setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("FPaymentItem (");

        sb.append(key);
        sb.append(", ").append(name);
        sb.append(", ").append(code);
        sb.append(", ").append(serial);
        sb.append(", ").append(amount);
        sb.append(", ").append(amountPre);
        sb.append(", ").append(settlementId);
        sb.append(", ").append(paymentId);
        sb.append(", ").append(payName);
        sb.append(", ").append(payMobile);
        sb.append(", ").append(payMethod);
        sb.append(", ").append(payId);
        sb.append(", ").append(sigma);
        sb.append(", ").append(language);
        sb.append(", ").append(active);
        sb.append(", ").append(metadata);
        sb.append(", ").append(createdAt);
        sb.append(", ").append(createdBy);
        sb.append(", ").append(updatedAt);
        sb.append(", ").append(updatedBy);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(IFPaymentItem from) {
        setKey(from.getKey());
        setName(from.getName());
        setCode(from.getCode());
        setSerial(from.getSerial());
        setAmount(from.getAmount());
        setAmountPre(from.getAmountPre());
        setSettlementId(from.getSettlementId());
        setPaymentId(from.getPaymentId());
        setPayName(from.getPayName());
        setPayMobile(from.getPayMobile());
        setPayMethod(from.getPayMethod());
        setPayId(from.getPayId());
        setSigma(from.getSigma());
        setLanguage(from.getLanguage());
        setActive(from.getActive());
        setMetadata(from.getMetadata());
        setCreatedAt(from.getCreatedAt());
        setCreatedBy(from.getCreatedBy());
        setUpdatedAt(from.getUpdatedAt());
        setUpdatedBy(from.getUpdatedBy());
    }

    @Override
    public <E extends IFPaymentItem> E into(E into) {
        into.from(this);
        return into;
    }
}
