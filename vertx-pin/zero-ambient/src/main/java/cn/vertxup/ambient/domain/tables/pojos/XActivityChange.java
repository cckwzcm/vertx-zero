/*
 * This file is generated by jOOQ.
 */
package cn.vertxup.ambient.domain.tables.pojos;


import cn.vertxup.ambient.domain.tables.interfaces.IXActivityChange;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.time.LocalDateTime;


import static io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo.*;
/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class XActivityChange implements VertxPojo, IXActivityChange {

    private static final long serialVersionUID = 1L;

    private String        key;
    private String        activityId;
    private String        type;
    private String        status;
    private String        fieldName;
    private String        fieldAlias;
    private String        fieldType;
    private String        valueOld;
    private String        valueNew;
    private String        sigma;
    private String        language;
    private Boolean       active;
    private String        metadata;
    private LocalDateTime createdAt;
    private String        createdBy;
    private LocalDateTime updatedAt;
    private String        updatedBy;

    public XActivityChange() {}

    public XActivityChange(IXActivityChange value) {
        this.key = value.getKey();
        this.activityId = value.getActivityId();
        this.type = value.getType();
        this.status = value.getStatus();
        this.fieldName = value.getFieldName();
        this.fieldAlias = value.getFieldAlias();
        this.fieldType = value.getFieldType();
        this.valueOld = value.getValueOld();
        this.valueNew = value.getValueNew();
        this.sigma = value.getSigma();
        this.language = value.getLanguage();
        this.active = value.getActive();
        this.metadata = value.getMetadata();
        this.createdAt = value.getCreatedAt();
        this.createdBy = value.getCreatedBy();
        this.updatedAt = value.getUpdatedAt();
        this.updatedBy = value.getUpdatedBy();
    }

    public XActivityChange(
        String        key,
        String        activityId,
        String        type,
        String        status,
        String        fieldName,
        String        fieldAlias,
        String        fieldType,
        String        valueOld,
        String        valueNew,
        String        sigma,
        String        language,
        Boolean       active,
        String        metadata,
        LocalDateTime createdAt,
        String        createdBy,
        LocalDateTime updatedAt,
        String        updatedBy
    ) {
        this.key = key;
        this.activityId = activityId;
        this.type = type;
        this.status = status;
        this.fieldName = fieldName;
        this.fieldAlias = fieldAlias;
        this.fieldType = fieldType;
        this.valueOld = valueOld;
        this.valueNew = valueNew;
        this.sigma = sigma;
        this.language = language;
        this.active = active;
        this.metadata = metadata;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

        public XActivityChange(io.vertx.core.json.JsonObject json) {
                this();
                fromJson(json);
        }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.KEY</code>. 「key」- 操作行为主键
     */
    @Override
    public String getKey() {
        return this.key;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.KEY</code>. 「key」- 操作行为主键
     */
    @Override
    public XActivityChange setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.ACTIVITY_ID</code>.
     * 「activityId」- 关联的操作ID
     */
    @Override
    public String getActivityId() {
        return this.activityId;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.ACTIVITY_ID</code>.
     * 「activityId」- 关联的操作ID
     */
    @Override
    public XActivityChange setActivityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.TYPE</code>. 「type」-
     * 字段变更类型：ADD | DELETE | UPDATE 三种
     */
    @Override
    public String getType() {
        return this.type;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.TYPE</code>. 「type」-
     * 字段变更类型：ADD | DELETE | UPDATE 三种
     */
    @Override
    public XActivityChange setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.STATUS</code>. 「status」-
     * 待确认变更状态：CONFIRMED | PENDING
     */
    @Override
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.STATUS</code>. 「status」-
     * 待确认变更状态：CONFIRMED | PENDING
     */
    @Override
    public XActivityChange setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_NAME</code>.
     * 「fieldName」- 如果是变更记录则需要生成变更日志
     */
    @Override
    public String getFieldName() {
        return this.fieldName;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_NAME</code>.
     * 「fieldName」- 如果是变更记录则需要生成变更日志
     */
    @Override
    public XActivityChange setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_ALIAS</code>.
     * 「fieldAlias」- 字段对应的别名
     */
    @Override
    public String getFieldAlias() {
        return this.fieldAlias;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_ALIAS</code>.
     * 「fieldAlias」- 字段对应的别名
     */
    @Override
    public XActivityChange setFieldAlias(String fieldAlias) {
        this.fieldAlias = fieldAlias;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_TYPE</code>.
     * 「fieldType」- 变更字段的数据类型，直接从模型定义中读取
     */
    @Override
    public String getFieldType() {
        return this.fieldType;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.FIELD_TYPE</code>.
     * 「fieldType」- 变更字段的数据类型，直接从模型定义中读取
     */
    @Override
    public XActivityChange setFieldType(String fieldType) {
        this.fieldType = fieldType;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.VALUE_OLD</code>.
     * 「valueOld」- 旧值
     */
    @Override
    public String getValueOld() {
        return this.valueOld;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.VALUE_OLD</code>.
     * 「valueOld」- 旧值
     */
    @Override
    public XActivityChange setValueOld(String valueOld) {
        this.valueOld = valueOld;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.VALUE_NEW</code>.
     * 「valueNew」- 新值
     */
    @Override
    public String getValueNew() {
        return this.valueNew;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.VALUE_NEW</code>.
     * 「valueNew」- 新值
     */
    @Override
    public XActivityChange setValueNew(String valueNew) {
        this.valueNew = valueNew;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.SIGMA</code>. 「sigma」-
     * 用户组绑定的统一标识
     */
    @Override
    public String getSigma() {
        return this.sigma;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.SIGMA</code>. 「sigma」-
     * 用户组绑定的统一标识
     */
    @Override
    public XActivityChange setSigma(String sigma) {
        this.sigma = sigma;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.LANGUAGE</code>.
     * 「language」- 使用的语言
     */
    @Override
    public String getLanguage() {
        return this.language;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.LANGUAGE</code>.
     * 「language」- 使用的语言
     */
    @Override
    public XActivityChange setLanguage(String language) {
        this.language = language;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.ACTIVE</code>. 「active」-
     * 是否启用
     */
    @Override
    public Boolean getActive() {
        return this.active;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.ACTIVE</code>. 「active」-
     * 是否启用
     */
    @Override
    public XActivityChange setActive(Boolean active) {
        this.active = active;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.METADATA</code>.
     * 「metadata」- 附加配置数据
     */
    @Override
    public String getMetadata() {
        return this.metadata;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.METADATA</code>.
     * 「metadata」- 附加配置数据
     */
    @Override
    public XActivityChange setMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.CREATED_AT</code>.
     * 「createdAt」- 创建时间
     */
    @Override
    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.CREATED_AT</code>.
     * 「createdAt」- 创建时间
     */
    @Override
    public XActivityChange setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.CREATED_BY</code>.
     * 「createdBy」- 创建人
     */
    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.CREATED_BY</code>.
     * 「createdBy」- 创建人
     */
    @Override
    public XActivityChange setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.UPDATED_AT</code>.
     * 「updatedAt」- 更新时间
     */
    @Override
    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.UPDATED_AT</code>.
     * 「updatedAt」- 更新时间
     */
    @Override
    public XActivityChange setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.UPDATED_BY</code>.
     * 「updatedBy」- 更新人
     */
    @Override
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.X_ACTIVITY_CHANGE.UPDATED_BY</code>.
     * 「updatedBy」- 更新人
     */
    @Override
    public XActivityChange setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("XActivityChange (");

        sb.append(key);
        sb.append(", ").append(activityId);
        sb.append(", ").append(type);
        sb.append(", ").append(status);
        sb.append(", ").append(fieldName);
        sb.append(", ").append(fieldAlias);
        sb.append(", ").append(fieldType);
        sb.append(", ").append(valueOld);
        sb.append(", ").append(valueNew);
        sb.append(", ").append(sigma);
        sb.append(", ").append(language);
        sb.append(", ").append(active);
        sb.append(", ").append(metadata);
        sb.append(", ").append(createdAt);
        sb.append(", ").append(createdBy);
        sb.append(", ").append(updatedAt);
        sb.append(", ").append(updatedBy);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(IXActivityChange from) {
        setKey(from.getKey());
        setActivityId(from.getActivityId());
        setType(from.getType());
        setStatus(from.getStatus());
        setFieldName(from.getFieldName());
        setFieldAlias(from.getFieldAlias());
        setFieldType(from.getFieldType());
        setValueOld(from.getValueOld());
        setValueNew(from.getValueNew());
        setSigma(from.getSigma());
        setLanguage(from.getLanguage());
        setActive(from.getActive());
        setMetadata(from.getMetadata());
        setCreatedAt(from.getCreatedAt());
        setCreatedBy(from.getCreatedBy());
        setUpdatedAt(from.getUpdatedAt());
        setUpdatedBy(from.getUpdatedBy());
    }

    @Override
    public <E extends IXActivityChange> E into(E into) {
        into.from(this);
        return into;
    }
}
