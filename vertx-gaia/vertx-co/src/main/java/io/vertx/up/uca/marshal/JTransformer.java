package io.vertx.up.uca.marshal;

import io.vertx.core.json.JsonObject;

public interface JTransformer<T> extends Transformer<JsonObject, T> {
}
