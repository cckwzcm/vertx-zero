/*
 * This file is generated by jOOQ.
 */
package cn.vertxup.ui.domain.tables.pojos;


import cn.vertxup.ui.domain.tables.interfaces.IUiControl;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.time.LocalDateTime;


import static io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo.*;
/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UiControl implements VertxPojo, IUiControl {

    private static final long serialVersionUID = 1L;

    private String        key;
    private String        sign;
    private String        pageId;
    private String        type;
    private String        containerName;
    private String        containerConfig;
    private String        assist;
    private String        grid;
    private String        componentName;
    private String        componentConfig;
    private String        componentData;
    private Boolean       active;
    private String        sigma;
    private String        metadata;
    private String        language;
    private LocalDateTime createdAt;
    private String        createdBy;
    private LocalDateTime updatedAt;
    private String        updatedBy;

    public UiControl() {}

    public UiControl(IUiControl value) {
        this.key = value.getKey();
        this.sign = value.getSign();
        this.pageId = value.getPageId();
        this.type = value.getType();
        this.containerName = value.getContainerName();
        this.containerConfig = value.getContainerConfig();
        this.assist = value.getAssist();
        this.grid = value.getGrid();
        this.componentName = value.getComponentName();
        this.componentConfig = value.getComponentConfig();
        this.componentData = value.getComponentData();
        this.active = value.getActive();
        this.sigma = value.getSigma();
        this.metadata = value.getMetadata();
        this.language = value.getLanguage();
        this.createdAt = value.getCreatedAt();
        this.createdBy = value.getCreatedBy();
        this.updatedAt = value.getUpdatedAt();
        this.updatedBy = value.getUpdatedBy();
    }

    public UiControl(
        String        key,
        String        sign,
        String        pageId,
        String        type,
        String        containerName,
        String        containerConfig,
        String        assist,
        String        grid,
        String        componentName,
        String        componentConfig,
        String        componentData,
        Boolean       active,
        String        sigma,
        String        metadata,
        String        language,
        LocalDateTime createdAt,
        String        createdBy,
        LocalDateTime updatedAt,
        String        updatedBy
    ) {
        this.key = key;
        this.sign = sign;
        this.pageId = pageId;
        this.type = type;
        this.containerName = containerName;
        this.containerConfig = containerConfig;
        this.assist = assist;
        this.grid = grid;
        this.componentName = componentName;
        this.componentConfig = componentConfig;
        this.componentData = componentData;
        this.active = active;
        this.sigma = sigma;
        this.metadata = metadata;
        this.language = language;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

        public UiControl(io.vertx.core.json.JsonObject json) {
                this();
                fromJson(json);
        }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.KEY</code>. 「key」- 主键
     */
    @Override
    public String getKey() {
        return this.key;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.KEY</code>. 「key」- 主键
     */
    @Override
    public UiControl setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.SIGN</code>. 「sign」- 控件使用的签名基本信息
     */
    @Override
    public String getSign() {
        return this.sign;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.SIGN</code>. 「sign」- 控件使用的签名基本信息
     */
    @Override
    public UiControl setSign(String sign) {
        this.sign = sign;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.PAGE_ID</code>. 「pageId」-
     * 当前控件所在的页面ID
     */
    @Override
    public String getPageId() {
        return this.pageId;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.PAGE_ID</code>. 「pageId」-
     * 当前控件所在的页面ID
     */
    @Override
    public UiControl setPageId(String pageId) {
        this.pageId = pageId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.TYPE</code>. 「type」-
     * 当前控件的类型：CONTAINER / COMPONENT / FORM / LIST，其中 FORM / LIST 需要访问子表
     */
    @Override
    public String getType() {
        return this.type;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.TYPE</code>. 「type」-
     * 当前控件的类型：CONTAINER / COMPONENT / FORM / LIST，其中 FORM / LIST 需要访问子表
     */
    @Override
    public UiControl setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.CONTAINER_NAME</code>.
     * 「containerName」- 当前控件使用的容器名
     */
    @Override
    public String getContainerName() {
        return this.containerName;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.CONTAINER_NAME</code>.
     * 「containerName」- 当前控件使用的容器名
     */
    @Override
    public UiControl setContainerName(String containerName) {
        this.containerName = containerName;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.CONTAINER_CONFIG</code>.
     * 「containerConfig」- 当前控件使用的容器配置
     */
    @Override
    public String getContainerConfig() {
        return this.containerConfig;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.CONTAINER_CONFIG</code>.
     * 「containerConfig」- 当前控件使用的容器配置
     */
    @Override
    public UiControl setContainerConfig(String containerConfig) {
        this.containerConfig = containerConfig;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.ASSIST</code>. 「assist」 -
     * 辅助数据（容器专用）
     */
    @Override
    public String getAssist() {
        return this.assist;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.ASSIST</code>. 「assist」 -
     * 辅助数据（容器专用）
     */
    @Override
    public UiControl setAssist(String assist) {
        this.assist = assist;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.GRID</code>. 「grid」 - 容器专用
     */
    @Override
    public String getGrid() {
        return this.grid;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.GRID</code>. 「grid」 - 容器专用
     */
    @Override
    public UiControl setGrid(String grid) {
        this.grid = grid;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_NAME</code>.
     * 「componentName」- 当前控件使用的组件名
     */
    @Override
    public String getComponentName() {
        return this.componentName;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_NAME</code>.
     * 「componentName」- 当前控件使用的组件名
     */
    @Override
    public UiControl setComponentName(String componentName) {
        this.componentName = componentName;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_CONFIG</code>.
     * 「componentConfig」- 当前控件使用的配置
     */
    @Override
    public String getComponentConfig() {
        return this.componentConfig;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_CONFIG</code>.
     * 「componentConfig」- 当前控件使用的配置
     */
    @Override
    public UiControl setComponentConfig(String componentConfig) {
        this.componentConfig = componentConfig;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_DATA</code>.
     * 「componentData」- 当前控件使用的数据，使用表达式结构
     */
    @Override
    public String getComponentData() {
        return this.componentData;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.COMPONENT_DATA</code>.
     * 「componentData」- 当前控件使用的数据，使用表达式结构
     */
    @Override
    public UiControl setComponentData(String componentData) {
        this.componentData = componentData;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public Boolean getActive() {
        return this.active;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public UiControl setActive(Boolean active) {
        this.active = active;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public String getSigma() {
        return this.sigma;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public UiControl setSigma(String sigma) {
        this.sigma = sigma;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.METADATA</code>. 「metadata」- 附加配置
     */
    @Override
    public String getMetadata() {
        return this.metadata;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.METADATA</code>. 「metadata」- 附加配置
     */
    @Override
    public UiControl setMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.LANGUAGE</code>. 「language」- 使用的语言
     */
    @Override
    public String getLanguage() {
        return this.language;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.LANGUAGE</code>. 「language」- 使用的语言
     */
    @Override
    public UiControl setLanguage(String language) {
        this.language = language;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.CREATED_AT</code>. 「createdAt」-
     * 创建时间
     */
    @Override
    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.CREATED_AT</code>. 「createdAt」-
     * 创建时间
     */
    @Override
    public UiControl setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.CREATED_BY</code>. 「createdBy」-
     * 创建人
     */
    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.CREATED_BY</code>. 「createdBy」-
     * 创建人
     */
    @Override
    public UiControl setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.UPDATED_AT</code>. 「updatedAt」-
     * 更新时间
     */
    @Override
    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.UPDATED_AT</code>. 「updatedAt」-
     * 更新时间
     */
    @Override
    public UiControl setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.UI_CONTROL.UPDATED_BY</code>. 「updatedBy」-
     * 更新人
     */
    @Override
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.UI_CONTROL.UPDATED_BY</code>. 「updatedBy」-
     * 更新人
     */
    @Override
    public UiControl setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("UiControl (");

        sb.append(key);
        sb.append(", ").append(sign);
        sb.append(", ").append(pageId);
        sb.append(", ").append(type);
        sb.append(", ").append(containerName);
        sb.append(", ").append(containerConfig);
        sb.append(", ").append(assist);
        sb.append(", ").append(grid);
        sb.append(", ").append(componentName);
        sb.append(", ").append(componentConfig);
        sb.append(", ").append(componentData);
        sb.append(", ").append(active);
        sb.append(", ").append(sigma);
        sb.append(", ").append(metadata);
        sb.append(", ").append(language);
        sb.append(", ").append(createdAt);
        sb.append(", ").append(createdBy);
        sb.append(", ").append(updatedAt);
        sb.append(", ").append(updatedBy);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(IUiControl from) {
        setKey(from.getKey());
        setSign(from.getSign());
        setPageId(from.getPageId());
        setType(from.getType());
        setContainerName(from.getContainerName());
        setContainerConfig(from.getContainerConfig());
        setAssist(from.getAssist());
        setGrid(from.getGrid());
        setComponentName(from.getComponentName());
        setComponentConfig(from.getComponentConfig());
        setComponentData(from.getComponentData());
        setActive(from.getActive());
        setSigma(from.getSigma());
        setMetadata(from.getMetadata());
        setLanguage(from.getLanguage());
        setCreatedAt(from.getCreatedAt());
        setCreatedBy(from.getCreatedBy());
        setUpdatedAt(from.getUpdatedAt());
        setUpdatedBy(from.getUpdatedBy());
    }

    @Override
    public <E extends IUiControl> E into(E into) {
        into.from(this);
        return into;
    }
}
