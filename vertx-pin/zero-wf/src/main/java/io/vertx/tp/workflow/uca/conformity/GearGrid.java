package io.vertx.tp.workflow.uca.conformity;

import cn.zeroup.macrocosm.cv.em.PassWay;

/**
 * @author <a href="http://www.origin-x.cn">Lang</a>
 */
public class GearGrid extends AbstractGear {
    public GearGrid() {
        super(PassWay.Grid);
    }
}
