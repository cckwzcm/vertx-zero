/*
 * This file is generated by jOOQ.
 */
package cn.vertxup.integration.domain.tables.pojos;


import cn.vertxup.integration.domain.tables.interfaces.IIPortfolio;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.time.LocalDateTime;


import static io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo.*;
/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class IPortfolio implements VertxPojo, IIPortfolio {

    private static final long serialVersionUID = 1L;

    private String        key;
    private String        name;
    private String        code;
    private String        status;
    private String        type;
    private String        integrationId;
    private String        ownerType;
    private String        owner;
    private String        runComponent;
    private String        runConfig;
    private String        dataKey;
    private String        dataConfig;
    private String        dataSecure;
    private String        dataIntegration;
    private String        appId;
    private Boolean       active;
    private String        sigma;
    private String        metadata;
    private String        language;
    private LocalDateTime createdAt;
    private String        createdBy;
    private LocalDateTime updatedAt;
    private String        updatedBy;

    public IPortfolio() {}

    public IPortfolio(IIPortfolio value) {
        this.key = value.getKey();
        this.name = value.getName();
        this.code = value.getCode();
        this.status = value.getStatus();
        this.type = value.getType();
        this.integrationId = value.getIntegrationId();
        this.ownerType = value.getOwnerType();
        this.owner = value.getOwner();
        this.runComponent = value.getRunComponent();
        this.runConfig = value.getRunConfig();
        this.dataKey = value.getDataKey();
        this.dataConfig = value.getDataConfig();
        this.dataSecure = value.getDataSecure();
        this.dataIntegration = value.getDataIntegration();
        this.appId = value.getAppId();
        this.active = value.getActive();
        this.sigma = value.getSigma();
        this.metadata = value.getMetadata();
        this.language = value.getLanguage();
        this.createdAt = value.getCreatedAt();
        this.createdBy = value.getCreatedBy();
        this.updatedAt = value.getUpdatedAt();
        this.updatedBy = value.getUpdatedBy();
    }

    public IPortfolio(
        String        key,
        String        name,
        String        code,
        String        status,
        String        type,
        String        integrationId,
        String        ownerType,
        String        owner,
        String        runComponent,
        String        runConfig,
        String        dataKey,
        String        dataConfig,
        String        dataSecure,
        String        dataIntegration,
        String        appId,
        Boolean       active,
        String        sigma,
        String        metadata,
        String        language,
        LocalDateTime createdAt,
        String        createdBy,
        LocalDateTime updatedAt,
        String        updatedBy
    ) {
        this.key = key;
        this.name = name;
        this.code = code;
        this.status = status;
        this.type = type;
        this.integrationId = integrationId;
        this.ownerType = ownerType;
        this.owner = owner;
        this.runComponent = runComponent;
        this.runConfig = runConfig;
        this.dataKey = dataKey;
        this.dataConfig = dataConfig;
        this.dataSecure = dataSecure;
        this.dataIntegration = dataIntegration;
        this.appId = appId;
        this.active = active;
        this.sigma = sigma;
        this.metadata = metadata;
        this.language = language;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

        public IPortfolio(io.vertx.core.json.JsonObject json) {
                this();
                fromJson(json);
        }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.KEY</code>. 「key」- 目录专用ID
     */
    @Override
    public String getKey() {
        return this.key;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.KEY</code>. 「key」- 目录专用ID
     */
    @Override
    public IPortfolio setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.NAME</code>. 「name」- 目录名称
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.NAME</code>. 「name」- 目录名称
     */
    @Override
    public IPortfolio setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.CODE</code>. 「code」- 目录系统编码
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.CODE</code>. 「code」- 目录系统编码
     */
    @Override
    public IPortfolio setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.STATUS</code>. 「status」- 目录状态
     */
    @Override
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.STATUS</code>. 「status」- 目录状态
     */
    @Override
    public IPortfolio setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.TYPE</code>. 「type」- 目录类型
     */
    @Override
    public String getType() {
        return this.type;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.TYPE</code>. 「type」- 目录类型
     */
    @Override
    public IPortfolio setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.INTEGRATION_ID</code>.
     * 「integrationId」- 是否关联集成配置，管理时直接同步
     */
    @Override
    public String getIntegrationId() {
        return this.integrationId;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.INTEGRATION_ID</code>.
     * 「integrationId」- 是否关联集成配置，管理时直接同步
     */
    @Override
    public IPortfolio setIntegrationId(String integrationId) {
        this.integrationId = integrationId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.OWNER_TYPE</code>. 「ownerType」-
     * 关联主体类型
     */
    @Override
    public String getOwnerType() {
        return this.ownerType;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.OWNER_TYPE</code>. 「ownerType」-
     * 关联主体类型
     */
    @Override
    public IPortfolio setOwnerType(String ownerType) {
        this.ownerType = ownerType;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.OWNER</code>. 「owner」- 关联主体主键
     */
    @Override
    public String getOwner() {
        return this.owner;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.OWNER</code>. 「owner」- 关联主体主键
     */
    @Override
    public IPortfolio setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.RUN_COMPONENT</code>.
     * 「runComponent」- 执行组件，LDAP执行专用
     */
    @Override
    public String getRunComponent() {
        return this.runComponent;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.RUN_COMPONENT</code>.
     * 「runComponent」- 执行组件，LDAP执行专用
     */
    @Override
    public IPortfolio setRunComponent(String runComponent) {
        this.runComponent = runComponent;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.RUN_CONFIG</code>. 「runConfig」-
     * 执行组件额外配置
     */
    @Override
    public String getRunConfig() {
        return this.runConfig;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.RUN_CONFIG</code>. 「runConfig」-
     * 执行组件额外配置
     */
    @Override
    public IPortfolio setRunConfig(String runConfig) {
        this.runConfig = runConfig;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_KEY</code>. 「dataKey」-
     * LDAP路径做完整标识
     */
    @Override
    public String getDataKey() {
        return this.dataKey;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_KEY</code>. 「dataKey」-
     * LDAP路径做完整标识
     */
    @Override
    public IPortfolio setDataKey(String dataKey) {
        this.dataKey = dataKey;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_CONFIG</code>. 「dataConfig」-
     * 数据基础配置
     */
    @Override
    public String getDataConfig() {
        return this.dataConfig;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_CONFIG</code>. 「dataConfig」-
     * 数据基础配置
     */
    @Override
    public IPortfolio setDataConfig(String dataConfig) {
        this.dataConfig = dataConfig;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_SECURE</code>. 「dataSecure」-
     * 安全专用配置
     */
    @Override
    public String getDataSecure() {
        return this.dataSecure;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_SECURE</code>. 「dataSecure」-
     * 安全专用配置
     */
    @Override
    public IPortfolio setDataSecure(String dataSecure) {
        this.dataSecure = dataSecure;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_INTEGRATION</code>.
     * 「dataIntegration」- 绑定好过后，导入/导出数据专用配置
     */
    @Override
    public String getDataIntegration() {
        return this.dataIntegration;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.DATA_INTEGRATION</code>.
     * 「dataIntegration」- 绑定好过后，导入/导出数据专用配置
     */
    @Override
    public IPortfolio setDataIntegration(String dataIntegration) {
        this.dataIntegration = dataIntegration;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.APP_ID</code>. 「appId」- 关联的应用程序ID
     */
    @Override
    public String getAppId() {
        return this.appId;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.APP_ID</code>. 「appId」- 关联的应用程序ID
     */
    @Override
    public IPortfolio setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public Boolean getActive() {
        return this.active;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.ACTIVE</code>. 「active」- 是否启用
     */
    @Override
    public IPortfolio setActive(Boolean active) {
        this.active = active;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public String getSigma() {
        return this.sigma;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.SIGMA</code>. 「sigma」- 统一标识
     */
    @Override
    public IPortfolio setSigma(String sigma) {
        this.sigma = sigma;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.METADATA</code>. 「metadata」- 附加配置
     */
    @Override
    public String getMetadata() {
        return this.metadata;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.METADATA</code>. 「metadata」- 附加配置
     */
    @Override
    public IPortfolio setMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.LANGUAGE</code>. 「language」-
     * 使用的语言
     */
    @Override
    public String getLanguage() {
        return this.language;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.LANGUAGE</code>. 「language」-
     * 使用的语言
     */
    @Override
    public IPortfolio setLanguage(String language) {
        this.language = language;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.CREATED_AT</code>. 「createdAt」-
     * 创建时间
     */
    @Override
    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.CREATED_AT</code>. 「createdAt」-
     * 创建时间
     */
    @Override
    public IPortfolio setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.CREATED_BY</code>. 「createdBy」-
     * 创建人
     */
    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.CREATED_BY</code>. 「createdBy」-
     * 创建人
     */
    @Override
    public IPortfolio setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.UPDATED_AT</code>. 「updatedAt」-
     * 更新时间
     */
    @Override
    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.UPDATED_AT</code>. 「updatedAt」-
     * 更新时间
     */
    @Override
    public IPortfolio setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Getter for <code>DB_ETERNAL.I_PORTFOLIO.UPDATED_BY</code>. 「updatedBy」-
     * 更新人
     */
    @Override
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    /**
     * Setter for <code>DB_ETERNAL.I_PORTFOLIO.UPDATED_BY</code>. 「updatedBy」-
     * 更新人
     */
    @Override
    public IPortfolio setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("IPortfolio (");

        sb.append(key);
        sb.append(", ").append(name);
        sb.append(", ").append(code);
        sb.append(", ").append(status);
        sb.append(", ").append(type);
        sb.append(", ").append(integrationId);
        sb.append(", ").append(ownerType);
        sb.append(", ").append(owner);
        sb.append(", ").append(runComponent);
        sb.append(", ").append(runConfig);
        sb.append(", ").append(dataKey);
        sb.append(", ").append(dataConfig);
        sb.append(", ").append(dataSecure);
        sb.append(", ").append(dataIntegration);
        sb.append(", ").append(appId);
        sb.append(", ").append(active);
        sb.append(", ").append(sigma);
        sb.append(", ").append(metadata);
        sb.append(", ").append(language);
        sb.append(", ").append(createdAt);
        sb.append(", ").append(createdBy);
        sb.append(", ").append(updatedAt);
        sb.append(", ").append(updatedBy);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(IIPortfolio from) {
        setKey(from.getKey());
        setName(from.getName());
        setCode(from.getCode());
        setStatus(from.getStatus());
        setType(from.getType());
        setIntegrationId(from.getIntegrationId());
        setOwnerType(from.getOwnerType());
        setOwner(from.getOwner());
        setRunComponent(from.getRunComponent());
        setRunConfig(from.getRunConfig());
        setDataKey(from.getDataKey());
        setDataConfig(from.getDataConfig());
        setDataSecure(from.getDataSecure());
        setDataIntegration(from.getDataIntegration());
        setAppId(from.getAppId());
        setActive(from.getActive());
        setSigma(from.getSigma());
        setMetadata(from.getMetadata());
        setLanguage(from.getLanguage());
        setCreatedAt(from.getCreatedAt());
        setCreatedBy(from.getCreatedBy());
        setUpdatedAt(from.getUpdatedAt());
        setUpdatedBy(from.getUpdatedBy());
    }

    @Override
    public <E extends IIPortfolio> E into(E into) {
        into.from(this);
        return into;
    }
}
