package io.vertx.up.runtime;

/**
 * @author <a href="http://www.origin-x.cn">Lang</a>
 */
public interface EnvVariables {
    // io debug for tracing
    String Z_IO_DEBUG = "Z_IO_DEBUG";
}
